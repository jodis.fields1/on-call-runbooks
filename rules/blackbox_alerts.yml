groups:
# Generic blackbox probe alerts.
- name: Blackbox Generic Probes
  rules:
  - alert: BlackboxProbeFailures
    expr: avg_over_time(probe_success{job="blackbox", instance!~"(https://ops.gitlab.net/users/sign_in|https://dev.gitlab.org.*|https://pre.gitlab.com|https://release.gitlab.net)"}[10m]) * 100 < 75
    labels:
      pager: pagerduty
      severity: s1
      alert_type: symptom
    annotations:
      description: blackbox probe availability {{ $labels.instance }} is less than {{ $value | printf "%.2f" }}% for the last 10 minutes.
      runbook: docs/blackbox/README.md
      title: Blackbox probes for {{ $labels.instance }} are failing.
  - alert: BlackboxProbeFailuresLong
    expr: avg_over_time(probe_success{job="blackbox", instance=~"(https://ops.gitlab.net/users/sign_in|https://dev.gitlab.org|https://pre.gitlab.com)"}[20m]) * 100 < 75
    labels:
      pager: pagerduty
      severity: s1
      alert_type: symptom
    annotations:
      description: blackbox probe availability {{ $labels.instance }} is less than {{ $value | printf "%.2f" }}% for the last 20 minutes.
      runbook: docs/blackbox/README.md
      title: Blackbox probes for {{ $labels.instance }} are failing.
  - alert: BlackboxProbeFailuresVeryLong
    expr: avg_over_time(probe_success{job="blackbox", instance=~"(https://release.gitlab.net)"}[1h]) * 100 < 75
    labels:
      pager: pagerduty
      severity: s1
      alert_type: symptom
    annotations:
      description: blackbox probe availability {{ $labels.instance }} is less than {{ $value | printf "%.2f" }}% for the last 1 hour.
      runbook: docs/blackbox/README.md
      title: Blackbox probes for {{ $labels.instance }} are failing.
- name: SSL Certificates
  rules:
  - record: domain:probe_ssl_earliest_cert_expiry:min
    expr: |
      min without (instance) (
        label_replace(probe_ssl_earliest_cert_expiry, "domain", "$1", "instance", "https://(([a-zA-Z0-9][a-zA-Z0-9-]{1,61}\\.?)+\\.[a-zA-Z]{2,})/.*")
      )
  - alert: SSLCertExpiresVerySoon
    expr: probe_ssl_earliest_cert_expiry - time() < 7 * 86400
    for: 30m
    labels:
      severity: s2
      alert_type: cause
      pager: pagerduty
    annotations:
      description: Check SSL for specified nodes and consider reissuing certificate.
      runbook: docs/frontend/ssl_cert.md
      title: SSL certificate for {{ $labels.instance }} expires in {{ $value | humanizeDuration
        }}
  - alert: SSLCertExpiresSoon
    expr: probe_ssl_earliest_cert_expiry - time() < 14 * 86400
    for: 30m
    labels:
      severity: s3
      alert_type: cause
    annotations:
      description: Check SSL for specified nodes and consider reissuing certificate.
      runbook: docs/frontend/ssl_cert.md
      expires: In {{ $value | humanizeDuration }}
      title: SSL certificate for {{ $labels.instance }} expires soon
# Customized blackbox probe alerts.
- name: Blackbox staging.gitlab.com
  rules:
  - alert: StagingGitlabComDown
    expr: probe_http_status_code{instance="https://staging.gitlab.com",job="blackbox-tls-redirect"} != 301
    for: 30m
    labels:
      pager: pagerduty
      severity: s1
      alert_type: symptom
    annotations:
      description: GitLab.com is down for more than 30 minutes!
      runbook: docs/frontend/gitlab-com-is-down.md
      title: staging.GitLab.com is down for 30 minutes
